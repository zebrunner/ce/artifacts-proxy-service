package com.zebrunner.aps.service;

import com.zebrunner.aps.domain.S3Object;

import java.util.Optional;

public interface ResizeService {

    Optional<S3Object> resize(S3Object image);

}
