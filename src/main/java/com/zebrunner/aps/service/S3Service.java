package com.zebrunner.aps.service;

import com.zebrunner.aps.domain.S3Object;

public interface S3Service {

    void upload(S3Object object);

}
