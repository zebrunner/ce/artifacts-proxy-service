package com.zebrunner.aps.service;

import com.zebrunner.aps.web.dto.ArtifactUploadedMessage;
import com.zebrunner.aps.web.dto.LogMessage;
import com.zebrunner.aps.web.dto.ScreenshotUploadedMessage;

public interface NotificationSender {

    boolean sendLog(LogMessage message);

    boolean sendScreenshot(ScreenshotUploadedMessage message);

    boolean sendArtifact(ArtifactUploadedMessage message);

}
