package com.zebrunner.aps.service.impl;

import com.zebrunner.aps.domain.S3Object;
import com.zebrunner.aps.service.NotificationSender;
import com.zebrunner.aps.service.ResizeService;
import com.zebrunner.aps.service.S3Service;
import com.zebrunner.aps.service.UploadService;
import com.zebrunner.aps.web.dto.ArtifactUploadedMessage;
import com.zebrunner.aps.web.dto.ScreenshotUploadedMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class UploadServiceImpl implements UploadService {

    private static final String TEST_RUN_KEY_FORMAT = "artifacts/test-runs/%s/%s";
    private static final String TEST_KEY_FORMAT = "artifacts/test-runs/%s/tests/%s/%s";
    private static final String PNG_EXTENSION = "png";

    private final S3Service s3Service;
    private final ResizeService resizeService;
    private final NotificationSender notificationSender;

    @Override
    public String storeScreenshot(byte[] screenshot, String tenant, String testRunId, String testId, long capturedAt) {
        long contentLength = screenshot.length;
        String filename = UUID.randomUUID().toString() + '.' + PNG_EXTENSION;
        String key = getKey(testRunId, testId, filename);

        S3Object screenshotAsContent = buildS3Object(screenshot, key, contentLength, MediaType.IMAGE_PNG_VALUE);
        s3Service.upload(screenshotAsContent);

        notificationSender.sendScreenshot(new ScreenshotUploadedMessage(tenant, testRunId, testId, key, capturedAt));

        resetContent(screenshotAsContent.getContent());
        resizeService.resize(screenshotAsContent)
                     .ifPresent(s3Service::upload);

        return key;
    }

    @Override
    public String storeArtifact(byte[] artifact, String filename, String contentType, String testRunId, String testId) {
        String key = getKey(testRunId, testId, filename);

        S3Object s3Object = buildS3Object(artifact, key, artifact.length, contentType);
        s3Service.upload(s3Object);

        notificationSender.sendArtifact(new ArtifactUploadedMessage(testRunId, testId, filename, '/' + key));

        return key;
    }

    private String getKey(String testRunId, String testId, String filename) {
        return testId != null
                ? String.format(TEST_KEY_FORMAT, testRunId, testId, filename)
                : String.format(TEST_RUN_KEY_FORMAT, testRunId, filename);
    }

    private S3Object buildS3Object(byte[] screenshot, String key, long contentLength, String contentType) {
        S3Object result = null;
        try (InputStream content = new ByteArrayInputStream(screenshot)) {
            content.mark(0);
            result = new S3Object(key, contentLength, contentType, content);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    private void resetContent(InputStream content) {
        try {
            content.reset();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

}
