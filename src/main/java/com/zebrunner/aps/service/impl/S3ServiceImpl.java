package com.zebrunner.aps.service.impl;

import com.zebrunner.aps.domain.S3Object;
import com.zebrunner.aps.service.S3Service;
import com.zebrunner.common.s3.AwsS3Properties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import software.amazon.awssdk.core.SdkResponse;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

@Component
@RequiredArgsConstructor
public class S3ServiceImpl implements S3Service {

    private final S3Client s3Client;
    private final AwsS3Properties awsS3Properties;

    @Override
    public void upload(S3Object object) {
        PutObjectRequest request = PutObjectRequest.builder()
                                                   .bucket(awsS3Properties.getBucket())
                                                   .key(object.getKey())
                                                   .contentType(object.getType())
                                                   .build();
        RequestBody requestBody = RequestBody.fromInputStream(object.getContent(), object.getLength());
        PutObjectResponse response = s3Client.putObject(request, requestBody);
        checkForErrors(response);
    }

    private void checkForErrors(SdkResponse response) {
        SdkHttpResponse httpResponse = response.sdkHttpResponse();
        if (httpResponse == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown upload exception");
        } else if (!httpResponse.isSuccessful()) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, "Upload failed with status " + httpResponse.statusCode()
            );
        }
    }

}
