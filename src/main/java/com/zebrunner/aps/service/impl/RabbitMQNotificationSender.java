package com.zebrunner.aps.service.impl;

import com.zebrunner.aps.configuration.Exchange;
import com.zebrunner.aps.configuration.RoutingKey;
import com.zebrunner.aps.service.NotificationSender;
import com.zebrunner.aps.web.dto.ArtifactUploadedMessage;
import com.zebrunner.aps.web.dto.LogMessage;
import com.zebrunner.aps.web.dto.ScreenshotUploadedMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class RabbitMQNotificationSender implements NotificationSender {

    private final RabbitTemplate rabbitTemplate;

    @Override
    public boolean sendLog(LogMessage message) {
        return send(message, Exchange.TEST_RUN_LOG, RoutingKey.SAVE_TEST_RUN_LOG);
    }

    @Override
    public boolean sendScreenshot(ScreenshotUploadedMessage message) {
        return send(message, Exchange.SCREENSHOT, "");
    }

    @Override
    public boolean sendArtifact(ArtifactUploadedMessage message) {
        return send(message, Exchange.ARTIFACT, RoutingKey.SAVE_ARTIFACT_REFERENCE);
    }

    private boolean send(Object message, String exchangeName, String routingKey) {
        try {
            rabbitTemplate.convertAndSend(exchangeName, routingKey, message);
            return true;
        } catch (AmqpException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }
}
