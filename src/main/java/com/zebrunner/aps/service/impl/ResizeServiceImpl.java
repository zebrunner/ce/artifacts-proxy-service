package com.zebrunner.aps.service.impl;

import com.zebrunner.aps.domain.S3Object;
import com.zebrunner.aps.service.ResizeService;
import com.zebrunner.aps.service.utils.ImageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.util.Optional;

@Slf4j
@Component
public class ResizeServiceImpl implements ResizeService {

    private final int thumbnailHeight;

    public ResizeServiceImpl(@Value("${thumbnail.height}") int thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
    }

    public Optional<S3Object> resize(S3Object image) {
        String key = image.getKey();
        log.info("Resizing image - key '{}'", key);
        String imageType = image.getType();
        String extension = ImageUtils.getExtensionFromMimeType(imageType);

        return ImageUtils.resize(image.getContent(), thumbnailHeight, extension)
                         .map(imageBytes -> toS3Object(key, imageType, imageBytes));
    }

    private S3Object toS3Object(String key, String imageType, byte[] imageBytes) {
        String thumbnailKey = createThumbnailKey(key);
        return new S3Object(thumbnailKey, imageBytes.length, imageType, new ByteArrayInputStream(imageBytes));
    }

    private static String createThumbnailKey(String imageKey) {
        int extensionDotIndex = imageKey.lastIndexOf(".");
        return imageKey.substring(0, extensionDotIndex) + "_thumbnail" + imageKey.substring(extensionDotIndex);
    }

}
