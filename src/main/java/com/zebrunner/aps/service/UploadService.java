package com.zebrunner.aps.service;

public interface UploadService {

    String storeScreenshot(byte[] screenshot, String tenant, String testRunId, String testId, long capturedAt);

    String storeArtifact(byte[] artifact, String filename, String contentType, String testRunId, String testId);

}
