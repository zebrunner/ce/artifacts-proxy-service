package com.zebrunner.aps.service.utils;

import lombok.extern.slf4j.Slf4j;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Slf4j
public class ImageUtils {

    private static final List<Scalr.Method> QUALITIES_PRIORITY = List.of(
            Scalr.Method.QUALITY, Scalr.Method.BALANCED, Scalr.Method.SPEED
    );

    static {
        ImageIO.setUseCache(false);
    }

    public static String getExtensionFromMimeType(String mimeType) {
        return mimeType.contains("/")
                ? mimeType.substring(mimeType.indexOf("/") + 1)
                : mimeType;
    }

    public static Optional<byte[]> resize(InputStream imageBytes, int thumbnailHeight, String extension) {
        try {
            BufferedImage image = ImageIO.read(imageBytes);
            int thumbnailWidth = recognizeThumbnailWidth(thumbnailHeight, image);

            return resizeImg(image, thumbnailWidth, thumbnailHeight, QUALITIES_PRIORITY.iterator())
                    .map(resizedImage -> toByteArray(resizedImage, extension));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return Optional.empty();
        }
    }

    private static Optional<BufferedImage> resizeImg(BufferedImage image, int width, int height, Iterator<Scalr.Method> qualitiesIterator) {
        if (!qualitiesIterator.hasNext()) {
            return Optional.empty();
        }
        Scalr.Method quality = qualitiesIterator.next();

        try {
            return Optional.ofNullable(Scalr.resize(image, quality, Scalr.Mode.FIT_TO_HEIGHT, width, height));
        } catch (Exception e) {
            log.error("Image scaling is not possible. " + e.getMessage(), e);
            return Optional.empty();
        } catch (OutOfMemoryError oom) {
            log.error(
                    "An OOM occurred during png resize from [{} x {}] to [{} x {}] with quality {}",
                    image.getHeight(), image.getWidth(), height, width, quality
            );
            return resizeImg(image, width, height, qualitiesIterator);
        }
    }

    private static int recognizeThumbnailWidth(double thumbnailHeight, BufferedImage image) {
        int height = image.getHeight();
        int width = image.getWidth();
        return (int) Math.ceil(width * (thumbnailHeight / height));
    }

    private static byte[] toByteArray(BufferedImage image, String extension) {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ImageIO.write(image, extension, output);
            return output.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

}
