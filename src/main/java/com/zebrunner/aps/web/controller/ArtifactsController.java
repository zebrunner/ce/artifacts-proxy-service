package com.zebrunner.aps.web.controller;

import com.zebrunner.aps.configuration.ArtifactProperties;
import com.zebrunner.aps.configuration.ArtifactProperties.FileConstraint;
import com.zebrunner.aps.service.NotificationSender;
import com.zebrunner.aps.service.UploadService;
import com.zebrunner.aps.web.dto.LogMessage;
import com.zebrunner.aps.web.dto.UploadResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Slf4j
@RestController
public class ArtifactsController {

    private static final String DEFAULT_SCHEMA_NAME = "zafira";
    private static final String SCREENSHOT_CAPTURED_AT = "x-zbr-screenshot-captured-at";
    private static final String TEST_RUN_ID_HEADER_NAME = "x-zbr-test-run-id";
    private static final String TEST_ID_HEADER_NAME = "x-zbr-test-id";
    private static final Set<String> SCREENSHOT_CONTENT_TYPES = Set.of("image/png", "image/jpeg");

    private final Tika tika;
    private final UploadService uploadService;
    private final NotificationSender rabbitSender;
    private final Map<String, DataSize> contentTypeToSize;

    public ArtifactsController(Tika tika,
                               UploadService uploadService,
                               NotificationSender rabbitSender,
                               ArtifactProperties artifactProperties) {
        this.tika = tika;
        this.uploadService = uploadService;
        this.rabbitSender = rabbitSender;

        this.contentTypeToSize = new HashMap<>();
        for (FileConstraint constraint : artifactProperties.getConstraints()) {
            for (String contentType : constraint.getContentTypes()) {
                DataSize maxSize = constraint.getMaxSize();
                contentTypeToSize.computeIfPresent(
                        contentType, ($, currentSize) -> maxSize.compareTo(currentSize) > 0 ? maxSize : currentSize
                );
                contentTypeToSize.putIfAbsent(contentType, maxSize);
            }
        }
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping("/v1/test-runs/{testRunId}/logs")
    public void relayLogMessage(@PathVariable("testRunId") String testRunId,
                                @RequestBody Collection<LogMessage> logs) {
        logs.forEach(log -> {
            fillMandatoryFields(testRunId, log);
            rabbitSender.sendLog(log);
        });
    }

    private void fillMandatoryFields(String testRunId, LogMessage log) {
        log.setTenant(DEFAULT_SCHEMA_NAME);
        log.setTestRunId(testRunId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/v1/screenshots")
    public UploadResult uploadPngImage(@RequestHeader(TEST_RUN_ID_HEADER_NAME) String testRunId,
                                       @RequestHeader(TEST_ID_HEADER_NAME) String testId,
                                       @RequestHeader(SCREENSHOT_CAPTURED_AT) Long capturedAt,
                                       @RequestBody byte[] bytes) {
        log.info("Screenshot received for test run {} and test {}", testRunId, testId);
        String contentType = validateContentType(bytes, SCREENSHOT_CONTENT_TYPES);
        validateContentLength(bytes.length, contentType);

        String key = uploadService.storeScreenshot(bytes, DEFAULT_SCHEMA_NAME, testRunId, testId, capturedAt);

        return new UploadResult(key);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/v1/artifacts", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public UploadResult uploadArtifact(@RequestHeader(TEST_RUN_ID_HEADER_NAME) String testRunId,
                                       @RequestHeader(name = TEST_ID_HEADER_NAME, required = false) String testId,
                                       @RequestParam("file") MultipartFile file) throws IOException {
        String filename = Optional.ofNullable(file.getOriginalFilename())
                                  .map(originalFileName -> originalFileName.replace(" ", "_"))
                                  .orElseGet(() -> UUID.randomUUID().toString());
        String contentType = validateContentType(file.getBytes(), contentTypeToSize.keySet());
        validateContentLength(file.getSize(), contentType);

        String key = uploadService.storeArtifact(file.getBytes(), filename, contentType, testRunId, testId);

        return new UploadResult(key);
    }

    private String validateContentType(byte[] bytes, Set<String> allowedContentTypes) {
        if (bytes != null) {
            String detectedMediaType = tika.detect(bytes);
            if (!allowedContentTypes.contains(detectedMediaType)) {
                log.error("Detected media type '{}' is not in the list of allowed ones {}", detectedMediaType, allowedContentTypes);
                throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Allowed media types are: " + allowedContentTypes);
            }
            return detectedMediaType;
        } else {
            return null;
        }
    }

    private void validateContentLength(long contentLength, String contentType) {
        if (contentLength < 0 || contentLength > contentTypeToSize.get(contentType).toBytes()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid file size (or Content-Length header is not set)");
        }
    }

}
