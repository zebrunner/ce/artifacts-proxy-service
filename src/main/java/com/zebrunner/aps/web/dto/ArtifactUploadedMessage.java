package com.zebrunner.aps.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArtifactUploadedMessage {

    private String testRunId;
    private String testId;
    private String name;
    private String key;

}
