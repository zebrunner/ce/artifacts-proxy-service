package com.zebrunner.aps.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScreenshotUploadedMessage {

    private String tenant;
    private String testRunId;
    private String testId;
    private String objectKey;
    private Long capturedAt;

}
