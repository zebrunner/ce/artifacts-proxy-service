package com.zebrunner.aps.web.dto;

import lombok.Data;

@Data
public class LogMessage {

    private String tenant;
    private String testRunId;
    private String testId;
    private Long timestamp;
    private String level;
    private String message;

}
