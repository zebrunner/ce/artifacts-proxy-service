package com.zebrunner.aps.configuration;

import com.zebrunner.common.s3.AwsS3Properties;
import org.apache.tika.Tika;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Utilities;

@Configuration
@EnableConfigurationProperties(ArtifactProperties.class)
public class Config {

    @Bean
    public S3Utilities s3Utilities(AwsS3Properties properties) {
        return S3Utilities.builder()
                          .region(Region.of(properties.getRegion()))
                          .build();
    }

    @Bean
    public Tika tika() {
        return new Tika();
    }

}
