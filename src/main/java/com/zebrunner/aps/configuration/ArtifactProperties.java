package com.zebrunner.aps.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.unit.DataSize;

import java.util.List;
import java.util.Set;

@Data
@ConfigurationProperties(prefix = "artifact")
public class ArtifactProperties {

    private List<FileConstraint> constraints = List.of();

    @Data
    public static class FileConstraint {

        private Set<String> contentTypes = Set.of();
        private DataSize maxSize = DataSize.ofMegabytes(100);

    }

}
