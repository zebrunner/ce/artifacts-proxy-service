package com.zebrunner.aps.configuration;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Exchange {

    public static final String SCREENSHOT = "reporting.test-run-screenshot";
    public static final String ARTIFACT = "reporting.test-run-artifact";
    public static final String TEST_RUN_LOG = "reporting.test-run-log";

}
