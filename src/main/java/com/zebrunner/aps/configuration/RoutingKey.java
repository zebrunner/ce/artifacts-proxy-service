package com.zebrunner.aps.configuration;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoutingKey {

    public static final String SAVE_ARTIFACT_REFERENCE = "save-reference";
    public static final String SAVE_TEST_RUN_LOG = "save";

}
