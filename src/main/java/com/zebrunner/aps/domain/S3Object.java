package com.zebrunner.aps.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class S3Object {

    private String key;
    private long length;
    private String type;
    private InputStream content;

}
