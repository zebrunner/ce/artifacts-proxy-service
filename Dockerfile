FROM adoptopenjdk:11-jre-hotspot
COPY build/libs/artifacts-proxy-service.jar /app/artifacts-proxy-service.jar
CMD ["java", "-jar", "/app/artifacts-proxy-service.jar"]
EXPOSE 8080
