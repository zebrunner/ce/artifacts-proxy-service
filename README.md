# Artifacts Proxy service

Artifacts proxy service used to process incoming log messages and screenshots from agents and distribute those to appropriate storages.

# Checking out and building

To check out the project and build from source, do the following:

    git clone git://github.com/zebrunner/artifacts-proxy-service.git
    cd artifacts-proxy-service
    ./gradlew build

# License

Artifacts Proxy service is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).
